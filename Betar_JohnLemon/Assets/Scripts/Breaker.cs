﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breaker : MonoBehaviour
{

    public Transform player;
    public GameObject theWall;
    bool m_IsPlayerInRange; //sets a boolean to see if the player is in the range of the door

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            //When the player enters the trigger, the player is in range. 
            m_IsPlayerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            //When the player exits the trigger, the player is no longer in range.
            m_IsPlayerInRange = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (m_IsPlayerInRange)
        {
            //if 'space' is pressed, john lemon can break down the door
            float pressed = Input.GetAxis("Break");
            if(pressed == 1.0f)
            {
                Destroy(theWall);
            }
        }   
    }

    // Start is called before the first frame update
    void Start()
    {

    }
}
