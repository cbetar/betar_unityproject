﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Observer : MonoBehaviour
{

    public Transform player;
    public Transform player2;
    public GameObject thisEnemy;
    public GameEnding gameEnding;
    public Text gargoyleSpeech;
    public bool alreadyFound;
    bool m_IsPlayerInRange;
    bool m_IsPlayer2InRange;
    bool m_PlayerInGargoyleRange;
    bool m_TextAppeared;

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform == player)
        {
            //When the player enters the trigger, the player is in range. 
            m_IsPlayerInRange = true;
        }
        if(other.transform == player2 && thisEnemy.CompareTag("Ghost"))
        {
            m_IsPlayer2InRange = true;
        }
        if (other.transform == player2 && thisEnemy.CompareTag("Gargoyle"))
        {
            m_PlayerInGargoyleRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            //When the player exits the trigger, the player is no longer in range.
            m_IsPlayerInRange = false;
        }
        if(other.transform == player2) //&& thisEnemy.CompareTag("Ghost"))
        {
            m_IsPlayer2InRange = false;
            m_PlayerInGargoyleRange = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

        inRanger(m_IsPlayerInRange, player, 1);
        inRanger(m_IsPlayer2InRange, player2, 2);
        textChanger(m_PlayerInGargoyleRange);
        
    }

    void inRanger(bool inRangePlayer, Transform thePlayer, int pNum)
    {
        //Checks if a player is in range, and catches the player if they get too close.
        if (inRangePlayer)
        {
            Vector3 direction = thePlayer.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction); //Creates a new ray with the 'new'
            RaycastHit raycastHit;
            if(Physics.Raycast(ray, out raycastHit))
            {
                if(raycastHit.collider.transform == thePlayer)
                {
                    gameEnding.CaughtPlayer(pNum);
                }
            }
        }
    }

    void textChanger(bool change)
    {
        if (change) 
        {
            //print("I CALLED SET:" + gameObject.transform.parent.gameObject.name);
            gargoyleSpeech.text = "If you find all of us gargoyles, we'll open up the exit.\n" + gameEnding.gs + "/3 found.";
            m_TextAppeared = true;
            //If  this is the first tine seeing the gargoyle, add count to game ending. 
            if (alreadyFound == false)
            {
                alreadyFound = true;
                gameEnding.gs += 1;
            }

        } else if(!change && m_TextAppeared == true)
        {
            //print("I CALLED CLEARING:" + gameObject.transform.parent.gameObject.name);
            gargoyleSpeech.text = "";
            m_TextAppeared = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        m_PlayerInGargoyleRange = false;
        gargoyleSpeech.text = "";
        alreadyFound = false;
    }
}
