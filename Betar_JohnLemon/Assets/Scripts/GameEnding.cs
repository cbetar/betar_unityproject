﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEnding : MonoBehaviour
{

    public float fadeDuration = 1f; //duration of the fade
    public GameObject player;
    public GameObject player2;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public CanvasGroup caughtBackgroundImageAltCanvasGroup;
    public float displayImageDuration = 1f;
    public int gs;
    public Text gameEnderText;
    bool m_IsPlayerAtExit;
    bool m_IsPlayer2AtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;
    int m_PlayerThatIsCaught;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player) // == checks for equivalency
        {
            m_IsPlayerAtExit = true;
        }
        if(other.gameObject == player2)
        {
            m_IsPlayer2AtExit = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject == player)
        {
            m_IsPlayerAtExit = false;
        }
        if(other.gameObject == player2)
        {
            m_IsPlayerAtExit = false;
        }
    }

    public void CaughtPlayer(int playerNum)
    {
        m_IsPlayerCaught = true;
        m_PlayerThatIsCaught = playerNum;
    } 

    // Start is called before the first frame update
    void Start()
    {
        gs = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_IsPlayerAtExit && m_IsPlayer2AtExit)
        {
            //If the player has not been caught, 'you escaped' scenario ensues.
            if (gs < 3)
            {
                gameEnderText.text = "Need to find the rest of the gargoyles! " + gs + "/3 found.";
            }
            else
            {
                EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
            }
        }
        else if (m_IsPlayerCaught)
        {
            if (m_PlayerThatIsCaught == 1)
            {
                //If the player has been caught, 'you were caught' and have to try again
                EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
            }
            else if (m_PlayerThatIsCaught == 2)
            {
                EndLevel(caughtBackgroundImageAltCanvasGroup, true, caughtAudio);
            }
        }
        if(!m_IsPlayerAtExit || !m_IsPlayer2AtExit)
        {
            gameEnderText.text = "";
        }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play(); //plays the audio
            m_HasAudioPlayed = true; //lets the script know the audio has played
        }
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        if(m_Timer > fadeDuration + displayImageDuration)
        {
            //restarts if caught, ends game if escape was successful
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else { Application.Quit(); }
        }
    }
}
